from fastapi import FastAPI

api = FastAPI()


@api.get("/health-check")
def obtain_health_check():
    return {"message": "Healthy"}
