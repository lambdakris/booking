import pytest

from starlette.testclient import TestClient

from src.main import api


@pytest.fixture
def client():
    return TestClient(api)


def test_health_check(client: TestClient):
    """
    Given the app is running
    When the following request is received
        method: GET
        address: /health-check
    Then the following response should be returned
        status: 200
        content: {
            "message": "Healthy"
        }
    """

    response = client.get("/health-check")

    assert response.status_code == 200
    assert response.json() == {"message": "Healthy"}
