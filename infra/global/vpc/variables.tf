variable "region" {
    description = "The AWS region where resources live"
    default = "us-east-2"
}

variable "public_subnet_1_cidr" {
    description = "cidr block for public subnet 1"
    default = "10.0.1.0/24"
}

variable "public_subnet_2_cidr" {
    description = "cidr block for public subnet 2"
    default = "10.0.2.0/24"
}

variable "private_subnet_1_cidr" {
    description = "cidr block for private subnet 1"
    default = "10.0.3.0/24"
}

variable "private_subnet_2_cidr" {
    description = "cidr block for private subnet 2"
    default = "10.0.4.0/24"
}

variable "availability_zones" {
    description = "list of availability zones"
    type = list(string)
    default = ["us-east-2a","us-east-2b"]
}