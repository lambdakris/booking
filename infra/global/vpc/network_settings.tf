# vpc

resource "aws_vpc" "booking_vpc" {
    cidr_block = "10.0.0.0/16"
    enable_dns_support = true
    enable_dns_hostnames = true
}

# public subnets

resource "aws_subnet" "public_subnet_1" {
    vpc_id = aws_vpc.booking_vpc.id
    cidr_block = var.public_subnet_1_cidr
    availability_zone = var.availability_zones[0]
}

resource "aws_subnet" "public_subnet_2" {
    vpc_id = aws_vpc.booking_vpc.id
    cidr_block = var.public_subnet_2_cidr
    availability_zone = var.availability_zones[1]
}

# private subnets

resource "aws_subnet" "private_subnet_1" {
    vpc_id = aws_vpc.booking_vpc.id
    cidr_block = var.private_subnet_1_cidr
    availability_zone = var.availability_zones[0]
}

resource "aws_subnet" "private_subnet_2" {
    vpc_id = aws_vpc.booking_vpc.id
    cidr_block = var.private_subnet_2_cidr
    availability_zone = var.availability_zones[1]
}

# public route table

resource "aws_route_table" "public_route_table" {
    vpc_id = aws_vpc.booking_vpc.id
}

resource "aws_route_table_association" "public_route_table_association_1" {
    route_table_id = aws_route_table.public_route_table.id
    subnet_id = aws_subnet.public_subnet_1.id
}

resource "aws_route_table_association" "public_route_table_association_2" {
    route_table_id = aws_route_table.public_route_table.id
    subnet_id = aws_subnet.public_subnet_2.id
}

# private route table

resource "aws_route_table" "private_route_table" {
    vpc_id = aws_vpc.booking_vpc.id
}

resource "aws_route_table_association" "private_route_table_association_1" {
    route_table_id = aws_route_table.private_route_table.id
    subnet_id = aws_subnet.private_subnet_1.id
}

resource "aws_route_table_association" "private_route_table_association_2" {
    route_table_id = aws_route_table.private_route_table.id
    subnet_id = aws_subnet.private_subnet_2.id
}

# internet gateway

resource "aws_internet_gateway" "internet_gw" {
    vpc_id = aws_vpc.booking_vpc.id
}
resource "aws_route" "internet_rt" {
    gateway_id = aws_internet_gateway.internet_gw.id
    route_table_id = aws_route_table.public_route_table.id
    destination_cidr_block = "0.0.0.0/0"
}

# nat gateway

resource "aws_eip" "nat_ip" {
    vpc = true
}
resource "aws_nat_gateway" "nat_gw" {
    allocation_id = aws_eip.nat_ip.id
    subnet_id = aws_subnet.public_subnet_1.id
}
resource "aws_route" "nat_rt" {
    nat_gateway_id = aws_nat_gateway.nat_gw.id
    route_table_id = aws_route_table.private_route_table.id
    destination_cidr_block = "0.0.0.0/0"
}