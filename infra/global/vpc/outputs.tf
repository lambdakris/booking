output "vpc_id" {
    value = aws_vpc.booking_vpc.id
}

output "alb_sg_id" {
    value = aws_security_group.alb_sg.id
}

output "ecs_sg_id" {
    value = aws_security_group.ecs_sg.id
}

output "public_subnet_1_id" {
    value = aws_subnet.public_subnet_1
}

output "public_subnet_2_id" {
    value = aws_subnet.public_subnet_2
}

output "private_subnet_1_id" {
    value = aws_subnet.private_subnet_1
}

output "private_subnet_2_id" {
    value = aws_subnet.private_subnet_2
}